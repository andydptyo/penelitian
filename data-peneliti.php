<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Data Peneliti</title>
  <link rel="stylesheet" href="assets/plugins/sweetalert2/sweetalert2.min.css">
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/plugins/DataTables/datatables.min.css">
  <link rel="stylesheet" href="assets/plugins/DataTables/DataTables-1.10.20/css/dataTables.bootstrap.min.css">

  <link href="assets/plugins/fontawesome-5.12.1/css/fontawesome.css" rel="stylesheet">
  <link href="assets/plugins/fontawesome-5.12.1/css/brands.css" rel="stylesheet">
  <link href="assets/plugins/fontawesome-5.12.1/css/solid.css" rel="stylesheet">

</head>

<body>
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <h2>Data
          <b>Peneliti</b>
        </h2>
      </div>
      <div class="col-sm-offset-6 col-sm-2">
        <div style="float: right; margin-top: 15px; margin-bottom: 15px;">
          <button class="btn btn-primary button-create" data-toggle="modal" data-target="#modal">
            <span class="glyphicon glyphicon-plus"></span>
            Insert New Data
          </button>
        </div>
      </div>
    </div>
    <div class="table-responsive">
      <table id="myTable" class="table table-striped table-bordered nowrap" width="100%">
        <thead>
          <tr class="active">
            <th>No</th>
            <th>NIK</th>
            <th>Nama Peneliti</th>
            <th>No Telepon</th>
            <th>Alamat Email</th>
            <th>Asal Instansi</th>
            <th>Edit</th>
            <th>Hapus</th>
          </tr>
        </thead>
        <tbody id="body">
        </tbody>
      </table>
    </div>
  </div>

  <!-- Modal 2 -->
  <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Data Peneliti</h4>
        </div>
        <div class="modal-body">
          <div class="container-fluid">
            <form id="form" class="form-horizontal">
              <!-- Section 1 -->
              <div class="panel panel-primary section">
                <div class="panel-heading">
                  <h3 class="panel-title" style="text-align: center; font-weight: bold;">Data Pribadi Peneliti</h3>
                </div>
                <div class="panel-body">
                  <div class="form-group form-group-sm">
                    <label for="nik" class="col-sm-2 control-label">NIK :</label>
                    <div class="col-sm-10">
                      <input type="text" name="nik" id="nik" class="form-control" placeholder="Your ID Number Here" maxlength="16" required="required">
                    </div>
                  </div>
                  <div class="form-group form-group-sm">
                    <label for="nama" class="col-sm-2 control-label">Nama :</label>
                    <div class="col-sm-10">
                      <input type="text" name="nama" id="nama" class="form-control" placeholder="Your Name Here" required="required">
                    </div>
                  </div>
                  <div class="form-group form-group-sm">
                    <label for="telepon" class="col-sm-2 control-label">Nomor Telepon :</label>
                    <div class="col-sm-10">
                      <input type="text" name="telepon" id="telepon" class="form-control" placeholder="Your Phone Number Here" required="required">
                    </div>
                  </div>
                  <div class="form-group form-group-sm">
                    <label for="email" class="col-sm-2 control-label">Alamat Email :</label>
                    <div class="col-sm-10">
                      <input type=" email" name="email" id="email" class="form-control" placeholder="email@example.com" required="required">
                    </div>
                  </div>
                  <div class="form-group form-group-sm">
                    <label for="pob" class="col-sm-2 control-label">Tempat Lahir :</label>
                    <div class="col-sm-10">
                      <input type="text" name="pob" id="pob" class="form-control" placeholder="Your Birthplace Here">
                    </div>
                  </div>
                  <div class="form-group form-group-sm">
                    <label for="dob" class="col-sm-2 control-label">Tanggal Lahir :</label>
                    <div class="col-sm-10">
                      <input type="date" name="dob" id="dob" class="form-control">
                    </div>
                  </div>
                  <div class="form-group form-group-sm">
                    <label for="sex" class="col-sm-2 control-label">Jenis Kelamin :</label>
                    <div class="col-sm-10">
                      <select name="sex" id="sex" class="form-control">
                        <option value="L">Laki-laki</option>
                        <option value="P">Perempuan</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Section 2 -->
              <div class="panel panel-primary section">
                <div class="panel-heading">
                  <h3 class="panel-title" style="text-align: center; font-weight: bold;">Pendidikan Peneliti</h3>
                </div>
                <div class="panel-body">
                  <div class="form-group form-group-sm">
                    <label for="pendidikan" class="col-sm-2 control-label">Pendidikan :</label>
                    <div class="col-sm-10">
                      <select name="pendidikan" id="pendidikan" class="form-control">
                        <option value="">-- Pilih Pendidikan --</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group form-group-sm">
                    <label for="instansi" class="col-sm-2 control-label">Asal Instansi :</label>
                    <div class="col-sm-10">
                      <input type="text" name="instansi" id="instansi" class="form-control" placeholder="Your University Here">
                    </div>
                  </div>
                  <div class="form-group form-group-sm">
                    <label for="fakultas" class="col-sm-2 control-label">Fakultas :</label>
                    <div class="col-sm-10">
                      <input type="text" name="fakultas" id="fakultas" class="form-control" placeholder="Your Faculty Here">
                    </div>
                  </div>
                  <div class="form-group form-group-sm">
                    <label for="jurusan" class="col-sm-2 control-label">Jurusan :</label>
                    <div class="col-sm-10">
                      <input type="text" name="jurusan" id="jurusan" class="form-control" placeholder="Your Majors Here">
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Section 3 -->
              <div class="panel panel-primary section">
                <div class="panel-heading">
                  <h3 class="panel-title" style="text-align: center; font-weight: bold;">Berkas Persyaratan</h3>
                </div>
                <div class="panel-body">
                  <div class="form-group form-group-sm">
                    <label for="upload" class="col-sm-2 control-label">Persyaratan 1 :</label>
                    <div class="form-group input-file" name="Fichier1">
                      <div class="col-sm-6">
                        <input id="upload" type="file" class="form-control" enctype="multipart/form-data" placeholder='Choose a file...' />
                      </div>
                      <div class="col-sm-2">
                        <button type="button" class="btn btn-danger btn-sm">
                          <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="modal-footer">
          <div>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" name="btn-update" class="btn btn-primary button-modal">Save changes</button>
          </div>
        </div>
      </div>
    </div>
  </div>

</body>

<script src="assets/plugins/sweetalert2/sweetalert2.all.min.js"></script>
<script src="assets/plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

<script src="assets/plugins/DataTables/datatables.min.js"></script>
<script src="assets/plugins/DataTables/DataTables-1.10.20/js/dataTables.bootstrap.min.js"></script>

<script src="assets/js/moment.min.js"></script>
<script src="assets/js/bootstrap-datetimepicker.min.js"></script>

<script>
<<<<<<< HEAD
  const baseUrl = 'http://localhost:9000/api_penelitian/';
=======
  const baseUrl = 'http://localhost/api_penelitian/';
>>>>>>> a09d7b12982eb1fc2db77b252c1dc07aafa82b69

  function getAll(url) {
    let i = 1;

    $.ajax({
      method: 'GET',
      url: url,
      success: function(response) {
        if (response.data) {
          let peneliti = response.data;

          peneliti.forEach((value, key) => {
            $("#body").append(
              "<tr>" +
              "<td>" + i++ + "</td>" +
              "<td>" + value['nik'] + "</td>" +
              "<td>" + value['nama_peneliti'] + "</td>" +
              "<td>" + value['telepon'] + "</td>" +
              "<td>" + value['email'] + "</td>" +
              "<td>" + value['instansi'] + "</td>" +
              '<td>' +
              '<button class="btn button-edit" data-toggle="modal" data-target="#modal" data-id="' + value['id'] + '">' +
              '<i class="fas fa-edit"></i>' +
              '</button>' +
              '</td>' +
              '<td>' +
              '<button class="btn button-delete" data-id="' + value['id'] + '" data-method="delete">' +
              '<i class="fas fa-trash-alt"></i>' +
              '</button>' +
              '</td>' +
              "</tr>"
            );
          });
        }
        $('#myTable').DataTable();
      }
    });
  }
<<<<<<< HEAD

  function store(url, data) {
    $.ajax({
      method: 'POST',
      url: url,
      data: data,
      dataType: 'json',
      success: function(response) {
        if (response.message == 'success') {
          Swal.fire({
            icon: 'success',
            title: 'Your work has been saved',
            showConfirmButton: true,
            // timer: 1500
          }).then((response) => {
            location.reload();
          });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: response.message
          })
        }
      }
    });
  }

  function getById(url) {
    $.ajax({
      method: "GET",
      url: url,
      success: function(response) {
        let result = response.data

        $("#nik").val(result['nik']);
        $("#nama").val(result['nama_peneliti']);
        $("#telepon").val(result['telepon']);
        $("#email").val(result['email']);
        $("#pob").val(result['pob']);
        $("#dob").val(result['dob']);
        $("#sex").val(result['sex']);
        $("#pendidikan").val(result['pendidikan']);
        $("#instansi").val(result['instansi']);
        $("#fakultas").val(result['fakultas']);
        $("#jurusan").val(result['jurusan']);
=======

  function store(url, data) {
    $.ajax({
      method: 'POST',
      url: url,
      data: data,
      dataType: 'json',
      success: function(response) {
        if (response.status == 201) {
          Swal.fire({
            icon: 'success',
            title: 'Your work has been saved',
            showConfirmButton: true,
            // timer: 1500
          }).then((response) => {
            location.reload();
          });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: response.message
          })
        }
>>>>>>> a09d7b12982eb1fc2db77b252c1dc07aafa82b69
      }
    });
  }

<<<<<<< HEAD
  function update(url, data) {
    $.ajax({
      method: 'PUT',
      // url: '/simrs/pendidikan_nondm/penelitian/process.php',
=======
  function getById(url) {
    $.ajax({
      method: "GET",
      url: url,
      success: function(response) {
        let result = response.data

        $("#nik").val(result['nik']);
        $("#nama").val(result['nama_peneliti']);
        $("#telepon").val(result['telepon']);
        $("#email").val(result['email']);
        $("#pob").val(result['pob']);
        $("#dob").val(result['dob']);
        $("#sex").val(result['sex']);
        $("#pendidikan").val(result['pendidikan']);
        $("#instansi").val(result['instansi']);
        $("#fakultas").val(result['fakultas']);
        $("#jurusan").val(result['jurusan']);
      }
    });
  }

  function update(url, data) {
    $.ajax({
      method: 'PUT',
>>>>>>> a09d7b12982eb1fc2db77b252c1dc07aafa82b69
      url: url,
      data: JSON.stringify(data),
      dataType: 'json',
      success: function(result) {
<<<<<<< HEAD
        if (result.status == 'failed') {
=======
        if (result.status == 204) {
>>>>>>> a09d7b12982eb1fc2db77b252c1dc07aafa82b69
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: result.message
          });
        } else {
          Swal.fire({
            icon: 'success',
            title: 'Your work has been saved',
            showConfirmButton: true

          }).then((result) => {
            location.reload();
          });
        }
      },
      error: function(err) {
        console.log(err);
      }
    });
  }

  function destroy(url) {
    $.ajax({
      method: 'DELETE',
      url: url,
      dataType: 'json',
      success: function(response) {
<<<<<<< HEAD
        if (response.message == 'success') {
=======
        if (response.status == 200) {
>>>>>>> a09d7b12982eb1fc2db77b252c1dc07aafa82b69
          Swal.fire(
            'Deleted!',
            response.message,
            'success',
          ).then((response) => {
            location.reload();
          });
        }
      }
    });
  }

  $(document).ready(function() {
    getAll(baseUrl + 'peneliti');
<<<<<<< HEAD
    // });
    // $(document).ready(function() {
    //   let i = 1;
    //   // const baseUrl = 'http://localhost/api_penelitian/';
    //   const baseUrl = 'http://localhost:9000/api_penelitian/';
    //   // let url = '/simrs/pendidikan_nondm/penelitian/load_process.php';
    //   // let peneliti = [];
    //   fetch(baseUrl + 'peneliti');
    //   /** Get all data peneliti */
    //   // $.ajax({
    //   //   method: 'GET',
    //   //   url: baseUrl + 'peneliti',
    //   //   success: function(response) {
    //   //     if (response.data) {
    //   //       let peneliti = response.data;

    //   //       peneliti.forEach((value, key) => {
    //   //         $("#body").append(
    //   //           "<tr>" +
    //   //           "<td>" + i++ + "</td>" +
    //   //           "<td>" + value['nik'] + "</td>" +
    //   //           "<td>" + value['nama_peneliti'] + "</td>" +
    //   //           "<td>" + value['telepon'] + "</td>" +
    //   //           "<td>" + value['email'] + "</td>" +
    //   //           "<td>" + value['instansi'] + "</td>" +
    //   //           '<td>' +
    //   //           '<button class="btn button-edit" data-toggle="modal" data-target="#modal" data-id="' + value['id'] + '">' +
    //   //           '<i class="fas fa-edit"></i>' +
    //   //           '</button>' +
    //   //           '</td>' +
    //   //           '<td>' +
    //   //           '<button class="btn button-delete" data-id="' + value['id'] + '" data-method="delete">' +
    //   //           '<i class="fas fa-trash-alt"></i>' +
    //   //           '</button>' +
    //   //           '</td>' +
    //   //           "</tr>"
    //   //         );
    //   //       });
    //   //     }
    //   //     $('#myTable').DataTable();
    //   //   }

    //   // });
=======
>>>>>>> a09d7b12982eb1fc2db77b252c1dc07aafa82b69

    /** Create Action */
    $(".button-create").click(function() {
      $(".button-modal").addClass('button-store');
      $("#form")[0].reset();

      /** Store Action */
      $("#modal").on('click', '.button-store', function(e) {
        e.preventDefault();
        let data = $("#form").serialize();
<<<<<<< HEAD
        // console.log(data);
        store(baseUrl + 'peneliti', data);
        // $.ajax({
        //   method: 'POST',
        //   // url: '/simrs/pendidikan_nondm/penelitian/process.php',
        //   url: baseUrl + 'peneliti',
        //   data: data,
        //   dataType: 'json',
        //   success: function(response) {
        //     if (response.message == 'success') {
        //       Swal.fire({
        //         icon: 'success',
        //         title: 'Your work has been saved',
        //         showConfirmButton: true,
        //         // timer: 1500
        //       }).then((response) => {
        //         location.reload();
        //       });
        //     } else {
        //       Swal.fire({
        //         icon: 'error',
        //         title: 'Oops...',
        //         text: response.message
        //       })
        //     }
        //   }
        // });
=======
        store(baseUrl + 'peneliti', data);
>>>>>>> a09d7b12982eb1fc2db77b252c1dc07aafa82b69
      });
    });

    /** Edit Action */
    $("#myTable").on('click', '.button-edit', function() {
      $(".button-modal").removeClass("button-store");
      $(".button-modal").addClass("button-update");

      let userId = $(this).data('id');

      /** Get data for edit form */
      getById(baseUrl + 'peneliti/' + userId);
<<<<<<< HEAD
      // $.ajax({
      //   method: "GET",
      //   url: baseUrl + 'peneliti/' + userId,
      //   success: function(response) {
      //     let result = response.data

      //     $("#nik").val(result['nik']);
      //     $("#nama").val(result['nama_peneliti']);
      //     $("#telepon").val(result['telepon']);
      //     $("#email").val(result['email']);
      //     $("#pob").val(result['pob']);
      //     $("#dob").val(result['dob']);
      //     $("#sex").val(result['sex']);
      //     $("#pendidikan").val(result['pendidikan']);
      //     $("#instansi").val(result['instansi']);
      //     $("#fakultas").val(result['fakultas']);
      //     $("#jurusan").val(result['jurusan']);
      //   }
      // });

      /** Update Action */
      $("#modal").on('click', '.button-update', function() {
        // let data = $("#form").serialize() + '&id=' + userId + '&method=put';
        // let data = $("#form").serializeArray();

=======

      /** Update Action */
      $("#modal").on('click', '.button-update', function() {
>>>>>>> a09d7b12982eb1fc2db77b252c1dc07aafa82b69
        let nik = $("#nik").val();
        let nama = $("#nama").val();
        let telepon = $("#telepon").val();
        let email = $("#email").val();
        let pob = $("#pob").val();
        let dob = $("#dob").val();
<<<<<<< HEAD
=======
        let sex = $("#sex").val();
>>>>>>> a09d7b12982eb1fc2db77b252c1dc07aafa82b69
        let pendidikan = $("#pendidikan").val();
        let instansi = $("#instansi").val();
        let fakultas = $("#fakultas").val();
        let jurusan = $("#jurusan").val();

        let data = {
          nik: nik,
          nama: nama,
          telepon: telepon,
          email: email,
          pob: pob,
          dob: dob,
<<<<<<< HEAD
=======
          sex: sex,
>>>>>>> a09d7b12982eb1fc2db77b252c1dc07aafa82b69
          pendidikan: pendidikan,
          instansi: instansi,
          fakultas: fakultas,
          jurusan: jurusan
        };

        update(baseUrl + 'peneliti/' + userId, data);
<<<<<<< HEAD
        // $.ajax({
        //   method: 'PUT',
        //   // url: '/simrs/pendidikan_nondm/penelitian/process.php',
        //   url: baseUrl + 'peneliti/' + userId,
        //   data: JSON.stringify(data),
        //   dataType: 'json',
        //   success: function(result) {
        //     if (result.status == 'failed') {
        //       Swal.fire({
        //         icon: 'error',
        //         title: 'Oops...',
        //         text: result.message
        //       });
        //     } else {
        //       Swal.fire({
        //         icon: 'success',
        //         title: 'Your work has been saved',
        //         showConfirmButton: true

        //       }).then((result) => {
        //         location.reload();
        //       });
        //     }
        //   },
        //   error: function(err) {
        //     console.log(err);
        //   }
        // });
=======
>>>>>>> a09d7b12982eb1fc2db77b252c1dc07aafa82b69
      });
    });

    /** Delete Action */
    $("#myTable").on('click', '.button-delete', function() {
      let userId = $(this).data('id');
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((response) => {
        if (response.value) {
          destroy(baseUrl + 'peneliti/' + userId);
<<<<<<< HEAD
          // $.ajax({
          //   method: 'DELETE',
          //   // url: '/simrs/pendidikan_nondm/penelitian/process.php',
          //   url: baseUrl + 'peneliti/' + userId,
          //   data: JSON.stringify({
          //     id: userId
          //   }),
          //   dataType: 'json',
          //   success: function(response) {
          //     if (response.message == 'success') {
          //       Swal.fire(
          //         'Deleted!',
          //         response.message,
          //         'success',
          //       ).then((response) => {
          //         location.reload();
          //       });
          //     }
          //   }
          // });
=======
>>>>>>> a09d7b12982eb1fc2db77b252c1dc07aafa82b69
        }
      })
    });

    //   /** Get the dropdown item for Form */
    //   $.ajax({
    //     method: 'GET',
    //     url: '/simrs/pendidikan_nondm/penelitian/class_master.php?get=pendidikan',
    //     success: function(result) {
    //       let data = JSON.parse(result);

    //       data.forEach((value, key) => {
    //         $("#pendidikan").append(
    //           "<option value='" + value[0] + "'>" +
    //           value[1] + "</option>"
    //         );
    //       });
    //     }
    //   });

  });
</script>

</html>