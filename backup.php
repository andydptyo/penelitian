<?php
$i = 1;
$query = "SELECT nik, nama_peneliti, telepon, email, instansi FROM peneliti";
$exec = mysql_query($query);

while ($data = mysql_fetch_array($exec)) { ?>
    <td><?php echo $i++; ?></td>
    <td><?php echo $data['nik']; ?></td>
    <td><?php echo $data['nama_peneliti']; ?></td>
    <td><?php echo $data['telepon']; ?></td>
    <td><?php echo $data['email']; ?></td>
    <td><?php echo $data['instansi']; ?></td>
    <td></td>
    </tbody>
<?php } ?>


<!-- Modal 2 -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Data Peneliti</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form id="form" class="form-horizontal" action="" method="">
                        <div class="row">
                            <div class="col-md-6">
                                <!-- Section 1 -->
                                <div class="panel panel-primary section-one">
                                    <div class="panel-heading">
                                        <h3 class="panel-title" style="text-align: center; font-weight: bold;">Data Pribadi Peneliti</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group form-group-sm">
                                            <label for="nik" class="col-sm-4 control-label">NIK :</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="nik" id="nik" class="form-control" placeholder="Your ID Number Here" maxlength="16" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-sm">
                                            <label for="nama" class="col-sm-4 control-label">Nama :</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="nama" id="nama" class="form-control" placeholder="Your Name Here" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-sm">
                                            <label for="telepon" class="col-sm-4 control-label">Nomor Telepon :</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="telepon" id="telepon" class="form-control" placeholder="Your Phone Number Here" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group form-group-sm">
                                            <label for="email" class="col-sm-4 control-label">Alamat Email :</label>
                                            <div class="col-sm-8">
                                                <input type=" email" name="email" id="email" class="form-control" placeholder="email@example.com" required>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-sm">
                                            <label for="pob" class="col-sm-4 control-label">Tempat Lahir :</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="pob" id="pob" class="form-control" placeholder="Your Birthplace Here">
                                            </div>
                                        </div>

                                        <div class="form-group form-group-sm">
                                            <label for="dob" class="col-sm-4 control-label">Tanggal Lahir :</label>
                                            <div class="col-sm-8">
                                                <input type="date" name="dob" id="dob" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group form-group-sm">
                                            <label for="sex" class="col-sm-4 control-label">Jenis Kelamin :</label>
                                            <div class="col-sm-8">
                                                <select name="sex" id="sex" class="form-control">
                                                    <option value="L">Laki-laki</option>
                                                    <option value="P">Perempuan</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <!-- Section 2 -->
                                <div class="panel panel-primary section-two">
                                    <div class="panel-heading">
                                        <h3 class="panel-title" style="text-align: center; font-weight: bold;">Pendidikan Peneliti</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group form-group-sm">
                                            <label for="pendidikan" class="col-sm-4 control-label">Pendidikan :</label>
                                            <div class="col-sm-8">
                                                <select name="pendidikan" id="pendidikan" class="form-control">
                                                    <option value="">-- Pilih Pendidikan --</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-sm">
                                            <label for="instansi" class="col-sm-4 control-label">Asal Instansi :</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="instansi" id="instansi" class="form-control" placeholder="Your University Here">
                                            </div>
                                        </div>

                                        <div class="form-group form-group-sm">
                                            <label for="fakultas" class="col-sm-4 control-label">Fakultas :</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="fakultas" id="fakultas" class="form-control" placeholder="Your Faculty Here">
                                            </div>
                                        </div>

                                        <div class="form-group form-group-sm">
                                            <label for="jurusan" class="col-sm-4 control-label">Jurusan :</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="jurusan" id="jurusan" class="form-control" placeholder="Your Majors Here">
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                Section 3
                                <div class="panel panel-primary section-three">
                                    <div class="panel-heading">
                                        <h3 class="panel-title" style="text-align: center; font-weight: bold;">Berkas Persyaratan</h3>
                                    </div>

                                    <div class="panel-body">
                                        <div class="form-group form-group-sm">
                                            <label for="upload" class="col-sm-4 control-label">Persyaratan 1 :</label>
                                            <div class="form-group input-file" name="Fichier1">
                                                <div class="col-sm-6">
                                                    <input id="upload" type="file" class="form-control" enctype="multipart/form-data" placeholder='Choose a file...' />
                                                </div>
                                                <div class="col-sm-2">
                                                    <button type="reset" class="btn btn-danger btn-sm">
                                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" name="btn-update" class="btn btn-primary button-modal">Save changes</button>
            </div>
        </div>
    </div>
</div> -->
<script>
    let currentSection = 0;
    showSection(currentSection);

    function showSection(n) {
        // This function will display the specified section of the form ...
        let x = document.getElementsByClassName("section");
        x[n].style.display = "block";
        // for the Prev/Next Button
        if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
        } else {
            document.getElementById("prevBtn").style.display = "inline";
        }
        if (n == (x.length - 1)) {
            document.getElementById("nextBtn").innerHTML = "Simpan";
        } else {
            document.getElementById("nextBtn").innerHTML = "Next";
        }
        // function for displays correct step indicator
        fixStepIndicator(n)
    }

    function nextPrev(n) {
        // This function will figure out which section to display
        let x = document.getElementsByClassName("section");
        // Exit the function if any field in the current tab is invalid
        // if (n == 1 && !validateForm()) {
        // return false;
        // }
        // Hide the current section
        x[currentSection].style.display = "none";
        // Increase or decrease the current section by 1
        currentSection = currentSection + n
        // When reached the end of the form
        if (currentSection >= x.length) {
            // the form gets submitted
            document.getElementById("forms").submit();
            return false;
        }
        // Otherwise, display the correct section
        showSection(currentSection);
    }

    function fixStepIndicator(n) {
        // This function removes the "active" class of all steps...
        let i, x = document.getElementsByClassName("step");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        } //... and adds the "active" class to the current step: x[n].className +=" active" ; } 

        function validateForm() { // This function deals with validation of the form fields 
            let x, y, i, valid = true;
            x = document.getElementsByClassName("section");
            y = x[currentTab].getElementsByTagName("input");
            // A loop that checks every input field in the current section: 
            for (i = 0; i < y.length; i++) {
                // If a field is empty... 
                if (y[i].value == "") {
                    // add an "invalid" class to the field: 
                    y[i].className += " invalid";
                    // and set the current valid status to false: valid=false;
                }
            }
            // If the valid status is true, mark the step as finished and valid: 
            if (valid) {
                document.getElementsByClassName("step")[currentTab].className += " finish";
            }
            return valid;
            // return the valid status 
        }
</script>