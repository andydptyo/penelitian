<?php
include 'config.php';

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    echo "<script>
    alert('Halaman ini tidak boleh diakses');
    window.location 'simrs/pendidikan_nondm/';
    </script>";
    die();
}

// var_dump($_POST);
// exit;

$nik = $_POST['nik'];
$nama = $_POST['nama'];
$telepon = $_POST['telepon'];
$email = $_POST['email'];
$pob = $_POST['pob'];
$dob = $_POST['dob'];
$sex = $_POST['sex'];
$pendidikan = $_POST['pendidikan'];
$instansi = $_POST['instansi'];
$fakultas = $_POST['fakultas'];
$jurusan = $_POST['jurusan'];
$id = $_POST['id'];
$method = $_POST['method'];

// Validasi untuk Proses Insert Data
if (
    (empty($_POST['nik'])
        || empty($_POST['nama'])
        || empty($_POST['telepon'])
        || empty($_POST['email']))
    && ($method !== 'delete')
) {
    echo json_encode(array('status' => 'failed', 'message' => 'Data tidak lengkap.'));
    die();
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $query = "INSERT INTO peneliti (
            nik,
            nama_peneliti,
            telepon,
            email,
            pob,
            dob,
            sex,
            pendidikan,
            instansi,
            fakultas,
            jurusan
        ) VALUES (
            '$nik',
            '$nama',
            '$telepon',
            '$email',
            '$pob',
            '$dob',
            '$sex',
            '$pendidikan',
            '$instansi',
            '$fakultas',
            '$jurusan'
        )";

    if ($method == 'delete') {
        $query = "DELETE FROM peneliti WHERE id='$id'";
    }

    if ($method == 'put') {
        $query = "UPDATE peneliti
        SET
            nik = '$nik',
            nama_peneliti = '$nama',
            telepon = '$telepon',
            email = '$email',
            pob = '$pob',
            dob = '$dob',
            sex = '$sex',
            pendidikan = '$pendidikan',
            instansi = '$instansi',
            fakultas = '$fakultas',
            jurusan = '$jurusan'
        WHERE
            id = '$id'";
    }
}

$fetch = mysql_query($query);

if (!$fetch) {
    echo json_encode(array('status' => 'failed', 'message' => 'Data gagal disimpan.'));
} else {
    if ($method == 'put') {
        echo json_encode(array('status' => 'success', 'message' => 'Data berhasil diubah.'));
    } else if ($method == 'delete') {
        echo json_encode(array('status' => 'success', 'message' => 'Data berhasil dihapus.'));
    } else {
        echo json_encode(array('status' => 'success', 'message' => 'Data berhasil disimpan.'));
    }
}
