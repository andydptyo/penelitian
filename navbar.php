    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="../penelitian/index.php">Modul Penelitian</a>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="../penelitian/index.php">Home</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Master
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Data Penelitian</a></li>
                        <li><a href="#">Data Akademik</a></li>
                        <li><a href="#">Data Keuangan</a></li>
                        <li><a href="#">Data Pengumpulan Laporan</a></li>
                        <li><a href="#">Data Pembimbing</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Penelitian
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="../penelitian/profil.php">Profil Peneliti</a></li>
                        <li><a href="#">Penjadwalan Peneliti</a></li>
                        <li><a href="#">Data Tanggungan</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Akademik
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Data Peneliti</a></li>
                        <li><a href="#">Pengumpulan Laporan</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Keuangan
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Pembayaran Institusi</a></li>
                        <li><a href="#">Penerimaan Pembayaran Institusi</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Logout</a></li>
            </ul>
        </div>
    </nav>