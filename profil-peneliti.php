<?php

include 'session.php';
include '../connect/konek.php';
include 'script.php';
// include 'head_menu.php';
// include 'header.php';

?>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profil Peneliti</title>
</head>

<body style="margin-left: 200px; margin-right: 150px; margin-top: 50px">
    <form id="form" class="form-horizontal" action="" method="">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title" style="text-align: center; font-weight: bold;">Data Pribadi Peneliti</h3>
            </div>
            <div class="panel-body">
                <div class="form-group form-group-sm">
                    <label for="nik" class="col-sm-2 control-label">NIK :</label>
                    <div class="col-sm-10">
                        <input type="text" name="nik" id="nik" class="form-control" placeholder="Your ID Number Here" maxlength="16" required="required">
                    </div>
                </div>

                <div class="form-group form-group-sm">
                    <label for="nama" class="col-sm-2 control-label">Nama :</label>
                    <div class="col-sm-10">
                        <input type="text" name="nama" id="nama" class="form-control" placeholder="Your Name Here" required="required">
                    </div>
                </div>

                <div class="form-group form-group-sm">
                    <label for="telepon" class="col-sm-2 control-label">Nomor Telepon :</label>
                    <div class="col-sm-10">
                        <input type="text" name="telepon" id="telepon" class="form-control" placeholder="Your Phone Number Here" required="required">
                    </div>
                </div>

                <div class="form-group form-group-sm">
                    <label for="email" class="col-sm-2 control-label"> Alamat Email :</label>
                    <div class="col-sm-10">
                        <input type="email" name="email" id="email" class="form-control" placeholder="email@example.com" required>
                    </div>
                </div>

                <div class="form-group form-group-sm">
                    <label for="pob" class="col-sm-2 control-label">Tempat Lahir :</label>
                    <div class="col-sm-10">
                        <input type="text" name="pob" id="pob" class="form-control" placeholder="Your Birthplace Here">
                    </div>
                </div>

                <div class="form-group form-group-sm">
                    <label for="dob" class="col-sm-2 control-label">Tanggal Lahir :</label>
                    <div class="col-sm-10">
                        <div class="input-group date" id="datepicker">
                            <input type="text" name="dob" class="form-control" id="dob" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="form-group form-group-sm">
                    <label for="sex" class="col-sm-2 control-label">Jenis Kelamin :</label>
                    <div class="col-sm-10">
                        <select name="sex" id="sex" class="form-control">
                            <option value="L">Laki-laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </div>
                </div>

            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title" style="text-align: center; font-weight: bold;">Pendidikan Peneliti</h3>
            </div>

            <div class="panel-body">
                <div class="form-group form-group-sm">
                    <label for="pendidikan" class="col-sm-2 control-label">Pendidikan :</label>
                    <div class="col-sm-10">
                        <select name="pendidikan" id="pendidikan" class="form-control">
                            <option value="">-- Pilih Pendidikan --</option>
                        </select>
                    </div>
                </div>

                <div class="form-group form-group-sm">
                    <label for="instansi" class="col-sm-2 control-label">Asal Instansi :</label>
                    <div class="col-sm-10">
                        <input type="text" name="instansi" id="instansi" class="form-control" placeholder="Your School Here">
                    </div>
                </div>

                <div class="form-group form-group-sm">
                    <label for="fakultas" class="col-sm-2 control-label">Fakultas :</label>
                    <div class="col-sm-10">
                        <select name="fakultas" id="fakultas" class="form-control">
                            <option value="">-- Select One --</option>
                        </select>
                    </div>
                </div>

                <div class="form-group form-group-sm">
                    <label for="jurusan" class="col-sm-2 control-label">Jurusan :</label>
                    <div class="col-sm-10">
                        <select name="jurusan" id="jurusan" class="form-control">
                            <option value="">-- Select One --</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group form-group-sm">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="button" id="buttonSubmit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>

</body>

<script type="text/javascript">
    $(document).ready(function() {
        // $(function() {
        //     $('#datepicker').datetimepicker({
        //         format: 'DD/MM/YYYY'
        //     });
        // });

        $.ajax({
            method: 'GET',
            url: '/simrs/pendidikan_nondm/penelitian/class_master.php?get=pendidikan',
            success: function(result) {
                let data = JSON.parse(result);

                data.forEach((value, key) => {
                    $("#pendidikan").append(
                        "<option value='" + value[0] + "'>" +
                        value[1] + "</option>"
                    );
                });
            }
        });

        $("#buttonSubmit").click(function(e) {
            e.preventDefault();
            let data = $("#form").serialize();
            // console.log(data);

            $.ajax({
                method: 'POST',
                url: '/simrs/pendidikan_nondm/penelitian/process.php',
                data: data,
                dataType: 'json',
                success: function(result) {
                    console.log(result.message);

                    if (result.status == 'failed') {
                        alert(result.message);
                    } else {
                        alert(result.message)
                    }

                }
            });
        });
    });
</script>

</html>