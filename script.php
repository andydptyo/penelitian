<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/DataTables/datatables.min.css">
<link rel="stylesheet" href="assets/DataTables/DataTables-1.10.20/css/dataTables.bootstrap.min.css">

<link href="assets/fontawesome-5.12.1/css/fontawesome.css" rel="stylesheet">
<link href="assets/fontawesome-5.12.1/css/brands.css" rel="stylesheet">
<link href="assets/fontawesome-5.12.1/css/solid.css" rel="stylesheet">

<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

<script src="assets/DataTables/datatables.min.js"></script>
<script src="assets/DataTables/DataTables-1.10.20/js/dataTables.bootstrap.min.js"></script>

<script src="assets/js/moment.min.js"></script>
<script src="assets/js/bootstrap-datetimepicker.min.js"></script>